package kz.zhanbolat.user;

import io.jsonwebtoken.Claims;
import kz.zhanbolat.user.controller.token.processor.TokenProcessor;
import kz.zhanbolat.user.exception.authorization.TokenNotFoundException;
import kz.zhanbolat.user.exception.authorization.RolesClaimNotFoundException;
import kz.zhanbolat.user.service.AuthService;
import kz.zhanbolat.user.stubs.HttpServletRequestCookieStub;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("test")
public class CookieTokenProcessorTest {
    @Value("${server.cookie.token.name}")
    private String tokenName;
    @Autowired
    private TokenProcessor<HttpServletRequest, Claims> cookieTokenProcessor;
    @Autowired
    private AuthService authService;

    @Test
    public void givenRequestWithoutAnyCookie_whenProcess_thenThrowException() {
        HttpServletRequest request = new HttpServletRequestCookieStub();

        assertThrows(TokenNotFoundException.class, () -> cookieTokenProcessor.process(request));
    }

    @Test
    public void givenRequestWithoutToken_whenProcess_thenThrowException() {
        HttpServletRequest request = new HttpServletRequestCookieStub("test", "test");

        assertThrows(TokenNotFoundException.class, () -> cookieTokenProcessor.process(request));
    }

    @Test
    public void givenRequestWithoutTokenRoleClaim_whenProcess_thenThrowException() {
        String token = authService.authenticateUser("test_user", "test_password");
        HttpServletRequest request = new HttpServletRequestCookieStub(tokenName, token);

        assertThrows(RolesClaimNotFoundException.class, () -> cookieTokenProcessor.process(request));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenRequestWithTokenRoleClaim_whenProcess_thenThrowException() {
        String token = authService.authenticateUser("test_student", "test_student");
        HttpServletRequest request = new HttpServletRequestCookieStub(tokenName, token);

        Claims claims = cookieTokenProcessor.process(request);
        List<String> roles = (List<String>) claims.get("roles");
        assertEquals(1, roles.size());
        assertTrue("student".equalsIgnoreCase(roles.get(0)));
    }
}
