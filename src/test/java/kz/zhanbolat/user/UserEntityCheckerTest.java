package kz.zhanbolat.user;

import kz.zhanbolat.user.entity.User;
import kz.zhanbolat.user.checker.UserEntityChecker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UserEntityCheckerTest {
    private UserEntityChecker userEntityChecker;
    private User user;

    @BeforeEach
    public void setUp() {
        userEntityChecker = new UserEntityChecker();
        user = new User();
    }

    @Test
    public void givenNull_whenCheckOnCreate_thenThrowExceptions() {
        assertThrows(Exception.class, () -> userEntityChecker.checkOnCreate(null));
    }

    @Test
    public void givenUserWithEmptyAndNullUsername_whenCheckOnCreate_thenThrowException() {
        user.setPassword("TEST");
        assertAll(() -> {
            assertThrows(Exception.class, () -> userEntityChecker.checkOnCreate(user));
            user.setUsername("");
            assertThrows(Exception.class, () -> userEntityChecker.checkOnCreate(user));
        });
    }

    @Test
    public void givenUserWithEmptyAndNullPassword_whenCheckOnCreate_thenThrowException() {
        user.setUsername("TEST");
        assertAll(() -> {
            assertThrows(Exception.class, () -> userEntityChecker.checkOnCreate(user));
            user.setPassword("");
            assertThrows(Exception.class, () -> userEntityChecker.checkOnCreate(user));
        });
    }

    @Test
    public void givenNull_whenCheckOnUpdate_thenThrowException() {
        assertThrows(Exception.class, () -> userEntityChecker.checkOnUpdate(null));
    }

    @Test
    public void givenUserNullOrBelowOrEqualsZeroId_whenCheckOnUpdate_thenThrowException() {
        user.setPassword("TEST");
        user.setUsername("TEST");
        assertAll(() -> {
            assertThrows(Exception.class, () -> userEntityChecker.checkOnUpdate(user));
            user.setId(0L);
            assertThrows(Exception.class, () -> userEntityChecker.checkOnUpdate(user));
            user.setId(-1L);
            assertThrows(Exception.class, () -> userEntityChecker.checkOnUpdate(user));
        });
    }

    @Test
    public void givenUserWithNullOrEmptyUsername_whenCheckOnUpdate_thenThrowException() {
        user.setId(1L);
        user.setPassword("TEST");
        assertAll(() -> {
            assertThrows(Exception.class, () -> userEntityChecker.checkOnCreate(user));
            user.setUsername("");
            assertThrows(Exception.class, () -> userEntityChecker.checkOnCreate(user));
        });
    }

    @Test
    public void givenUserWithNullOrEmptyPassword_whenCheckOnUpdate_thenThrowException() {
        user.setId(1L);
        user.setUsername("TEST");
        assertAll(() -> {
            assertThrows(Exception.class, () -> userEntityChecker.checkOnCreate(user));
            user.setPassword("");
            assertThrows(Exception.class, () -> userEntityChecker.checkOnCreate(user));
        });
    }
}
