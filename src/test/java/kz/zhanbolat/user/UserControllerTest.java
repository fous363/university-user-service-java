package kz.zhanbolat.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import kz.zhanbolat.user.entity.User;
import kz.zhanbolat.user.service.AuthService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.Cookie;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = TestConfiguration.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AuthService authService;

    private ObjectMapper objectMapper;

    @Value("${server.cookie.token.name}")
    private String tokenName;

    @BeforeEach
    public void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    public void givenExistedUserId_whenGetUser_thenReturnUser() throws Exception {
        String studentToken = authService.authenticateUser("test_student", "test_student");

        mockMvc.perform(get("/api/user/1").cookie(new Cookie(tokenName, studentToken)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.username").value("test_user"));
    }

    @Test
    public void givenNotExistedUserId_whenGetUser_thenReturnErrorResponse() throws Exception {
        String studentToken = authService.authenticateUser("test_student", "test_student");

        mockMvc.perform(get("/api/user/100").cookie(new Cookie(tokenName, studentToken)))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    public void givenNotExistedUserWithRoleStudent_whenCreateUser_thenReturnUserWithStudentRole() throws Exception {
        User user = new User();
        user.setUsername("test_user_from_rest");
        user.setPassword("test_password");

        String jsonUser = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/api/user/student")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonUser))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.username").value(user.getUsername()))
                .andExpect(jsonPath("$.roles").isNotEmpty())
                .andExpect(jsonPath("$.roles[0].id").value(1))
                .andExpect(jsonPath("$.roles[0].name").value("student"));
    }

    @Test
    public void givenNotExistedUserWithRoleLecturer_whenCreateUser_thenReturnUserWithLecturerRole() throws Exception {
        User user = new User();
        user.setUsername("test_lecturer_from_rest");
        user.setPassword("test_password");

        String jsonUser = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/api/user/lecturer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUser))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.username").value(user.getUsername()))
                .andExpect(jsonPath("$.roles").isNotEmpty())
                .andExpect(jsonPath("$.roles[0].id").value(2))
                .andExpect(jsonPath("$.roles[0].name").value("lecturer"));
    }

    @Test
    public void givenExistedUser_whenCreateUser_thenReturnErrorResponse() throws Exception {
        User user = new User();
        user.setId(1L);
        user.setUsername("test_user");
        user.setPassword("test_password");

        String jsonUser = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/api/user/student")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUser))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    public void givenDuplicatedUsername_whenCreateUser_thenReturnErrorResponse() throws Exception {
        User user = new User();
        user.setUsername("test_user");
        user.setPassword("test_password");

        String jsonUser = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/api/user/student")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUser))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    public void givenExistedUser_whenUpdateUser_thenReturnUser() throws Exception {
        String adminToken = authService.authenticateUser("test_admin", "test_admin");
        User user = new User();
        user.setId(2L);
        user.setUsername("test");
        user.setPassword("test");

        String jsonUser = objectMapper.writeValueAsString(user);

        mockMvc.perform(put("/api/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonUser)
                        .cookie(new Cookie(tokenName, adminToken)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(user.getId()))
                .andExpect(jsonPath("$.username").value(user.getUsername()));
    }

    @Test
    public void givenNotExistedUser_whenUpdateUser_thenReturnErrorResponse() throws Exception {
        String adminToken = authService.authenticateUser("test_admin", "test_admin");
        User user = new User();
        user.setId(100L);
        user.setUsername("test");
        user.setPassword("test");

        String jsonUser = objectMapper.writeValueAsString(user);

        mockMvc.perform(put("/api/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUser).cookie(new Cookie(tokenName, adminToken)))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    public void givenExistedUser_whenAuthUser_thenReturnUser() throws Exception {
        String adminToken = authService.authenticateUser("test_admin", "test_admin");
        User user = new User();
        user.setUsername("test_user");
        user.setPassword("test_password");

        String jsonUser = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/api/user/auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(jsonUser)
                    .cookie(new Cookie(tokenName, adminToken)))
                .andExpect(status().isOk())
                .andExpect(cookie().exists("access-token"));
    }

    @Test
    public void givenNotExistedUser_whenAuthUser_thenReturnErrorResponse() throws Exception {
        User user = new User();
        user.setUsername("test_not_existed_user");
        user.setPassword("test_password");

        String jsonUser = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/api/user/auth")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(jsonUser))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    public void givenCookiesWithoutToken_whenValidateAuthorizationToken_thenReturnAuthorizationResponse() throws Exception {
        mockMvc.perform(post("/api/user/validate/token"))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.authorized").value(false));
    }

    @Test
    public void givenTokenWithoutClaim_whenValidateAuthorizationToken_thenReturnAuthorizationResponse() throws Exception {
        String token = authService.authenticateUser("test_user", "test_password");

        mockMvc.perform(post("/api/user/validate/token").cookie(new Cookie(tokenName, token)))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.authorized").value(false));
    }

    @Test
    public void givenTokenWithClaim_whenValidateAuthorizationToken_thenReturnAuthorizationResponse() throws Exception {
        List<String> expectedAuthorizationRoles = new ArrayList<>();
        expectedAuthorizationRoles.add("STUDENT");
        String token = authService.authenticateUser("test_student", "test_student");

        mockMvc.perform(post("/api/user/validate/token").cookie(new Cookie(tokenName, token)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.authorized").value(true))
                .andExpect(jsonPath("$.authorizationSubject").value("test_student"))
                .andExpect(jsonPath("$.authorizationRoles").value(expectedAuthorizationRoles));
    }
}
