package kz.zhanbolat.user;

import kz.zhanbolat.user.controller.token.generator.TokenCookieGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("test")
public class AccessTokenCookieGeneratorTest {
    @Autowired
    private TokenCookieGenerator<String, String> cookieGenerator;

    @Test
    public void givenEmptyToken_whenGenerate_thenThrowException() {
        assertAll(() -> {
            assertThrows(IllegalArgumentException.class, () -> cookieGenerator.generate(null));
            assertThrows(IllegalArgumentException.class, () -> cookieGenerator.generate(""));
        });
    }

    @Test
    public void givenToken_whenGenerate_thenReturnString() {
        String expectedCookie = "access-token=token; Max-Age=3600; Domain=www.domain.com; Path=/; Secure; HttpOnly; SameSite=None";
        String token = cookieGenerator.generate("token");

        assertEquals(expectedCookie, token);
    }
}
