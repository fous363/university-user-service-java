package kz.zhanbolat.user;

import kz.zhanbolat.user.controller.dto.UserResponse;
import kz.zhanbolat.user.controller.mapper.UserResponseMapper;
import kz.zhanbolat.user.entity.Role;
import kz.zhanbolat.user.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class UserResponseMapperTest {
    private UserResponseMapper mapper;

    @BeforeEach
    public void setUp() {
        mapper = new UserResponseMapper();
    }

    @Test
    public void givenNull_whenMap_thenThrowException() {
        assertThrows(Exception.class, () -> mapper.map(null));
    }

    @Test
    public void givenEmptyUser_whenMap_thenReturnUserResponse() {
        UserResponse userResponse = mapper.map(new User());

        assertNull(userResponse.getId());
        assertNull(userResponse.getUsername());
        assertEquals(Collections.emptyList(), userResponse.getRoles());
    }

    @Test
    public void givenUser_whenMap_thenReturnUserResponse() {
        User user = new User();
        user.setId(1L);
        user.setUsername("TEST");
        user.setRoles(Collections.singletonList(new Role()));

        UserResponse userResponse = mapper.map(user);

        assertEquals(user.getId(), userResponse.getId());
        assertEquals(user.getUsername(), userResponse.getUsername());
        assertEquals(user.getRoles(), userResponse.getRoles());
    }
}
