package kz.zhanbolat.user;

import kz.zhanbolat.user.entity.RoleType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RoleTypeTest {

    @Test
    public void givenRolesName_whenGetRoleTypeByName_thenReturnEachRoleType() {
        assertAll(() -> {
            assertEquals(RoleType.STUDENT, RoleType.getRoleTypeByName("student"));
            assertEquals(RoleType.LECTURER, RoleType.getRoleTypeByName("lecturer"));
        });
    }

    @Test
    public void givenNotExistingRoleName_whenGetRoleTypeByName_thenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> RoleType.getRoleTypeByName("not-existing"));
    }
}
