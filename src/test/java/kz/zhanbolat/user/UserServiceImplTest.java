package kz.zhanbolat.user;

import kz.zhanbolat.user.entity.RoleType;
import kz.zhanbolat.user.entity.User;
import kz.zhanbolat.user.exception.ServiceException;
import kz.zhanbolat.user.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("test")
public class UserServiceImplTest {
    @Autowired
    private UserService userService;

    @Test
    public void givenExistedUserId_whenGetUser_thenReturnUser() {
        User expectedUser = new User();
        expectedUser.setId(1L);
        expectedUser.setUsername("test_user");
        expectedUser.setPassword("test_password");
        User user = userService.getUser(1L);

        assertEquals(expectedUser, user);
    }

    @Test
    public void givenNotExistedUserId_whenGetUser_thenThrowException() {
        assertThrows(ServiceException.class, () -> userService.getUser(100L));
    }

    @Test
    public void givenNull_whenGetUser_thenThrowException() {
        assertThrows(Exception.class, () -> userService.getUser(null));
    }

    @Test
    public void givenExistingUserId_whenCreateUser_thenThrowException() {
        User user = new User();
        user.setId(1L);
        user.setUsername("test");
        user.setPassword("test");

        assertThrows(ServiceException.class, () -> userService.createUser(user, RoleType.STUDENT));
    }

    @Test
    public void givenExistingUsernameAndPassword_whenCreateUser_thenThrowException() {
        User user = new User();
        user.setUsername("test_user");
        user.setPassword("test_password");

        assertThrows(ServiceException.class, () -> userService.createUser(user, RoleType.STUDENT));
    }

    @Test
    public void givenNotExitedUserWithRoleStudent_whenCreateUser_thenReturnUserWithStudentRole() {
        User newUser = new User();
        newUser.setUsername("new-user");
        newUser.setPassword("password");
        User user = userService.createUser(newUser, RoleType.STUDENT);

        assertNotNull(user.getId());
        assertEquals(newUser.getUsername(), user.getUsername());
        assertEquals(newUser.getPassword(), user.getPassword());
        assertEquals(RoleType.STUDENT.getId(), user.getRoles().get(0).getId());
    }

    @Test
    public void givenNotExistedUserWithRoleLecturer_whenCreateUser_thenReturnUserWithLecturerRole() {
        User newUser = new User();
        newUser.setUsername("lecturer-new-user");
        newUser.setPassword("password");
        User user = userService.createUser(newUser, RoleType.LECTURER);

        assertNotNull(user.getId());
        assertEquals(newUser.getUsername(), user.getUsername());
        assertEquals(newUser.getPassword(), user.getPassword());
        assertEquals(RoleType.LECTURER.getId(), user.getRoles().get(0).getId());
    }

    @Test
    public void givenExistedUser_whenCreateUser_thenThrowException() {
        User existedUser = new User();
        existedUser.setId(1L);

        assertThrows(IllegalArgumentException.class, () -> userService.createUser(existedUser, RoleType.STUDENT));
    }

    @Test
    public void givenUserWithExistedUsername_whenCreateUser_thenThrowException() {
        User user = new User();
        user.setUsername("test_user");

        assertThrows(Exception.class, () -> userService.createUser(user, RoleType.STUDENT));
    }

    @Test
    public void givenNull_whenCreateUser_thenThrowException() {
        assertThrows(Exception.class, () -> userService.createUser(null, RoleType.STUDENT));
    }

    @Test
    public void givenEmptyUser_whenCreateUser_thenThrowException() {
        User user = new User();
        user.setPassword("");
        user.setUsername("");

        assertThrows(IllegalArgumentException.class, () -> userService.createUser(new User(), RoleType.STUDENT));
        assertThrows(IllegalArgumentException.class, () -> userService.createUser(user, RoleType.STUDENT));
    }

    @Test
    public void givenNullRoleType_whenCreateUser_thenThrowException() {
        User user = new User();
        user.setPassword("empty-role");
        user.setUsername("empty-role");

        assertThrows(Exception.class, () -> userService.createUser(user, null));
    }

    @Test
    public void givenExistedUser_whenUpdateUser_thenReturnUser() {
        User exitedUser = userService.getUser(2L);
        // clone of existed user, because it's in persistence context
        User clone = new User();
        clone.setId(exitedUser.getId());
        clone.setUsername(exitedUser.getUsername());
        clone.setPassword(exitedUser.getPassword());

        User user = new User();
        user.setId(2L);
        user.setUsername("test");
        user.setPassword("test");

        User updatedUser = userService.updateUser(user);

        assertEquals(clone.getId(), updatedUser.getId());
        assertNotEquals(clone.getUsername(), updatedUser.getUsername());
        assertNotEquals(clone.getPassword(), updatedUser.getPassword());
    }

    @Test
    public void givenNull_whenUpdateUser_thenThrowException() {
        assertThrows(Exception.class, () -> userService.updateUser(null));
    }

    @Test
    public void givenNotExistedUser_whenUpdateUser_thenThrowException() {
        User user = new User();
        user.setPassword("test");
        user.setUsername("test");

        assertThrows(NullPointerException.class, () -> userService.updateUser(user));
    }

    @Test
    public void givenEmptyUser_whenUpdateUser_thenThrowException() {
        User emptyUsername = new User();
        emptyUsername.setId(2L);
        emptyUsername.setUsername("");
        emptyUsername.setPassword("test");

        User nullUsername = new User();
        nullUsername.setId(2L);
        nullUsername.setPassword("test");

        User nullId = new User();
        nullId.setUsername("test");
        nullId.setPassword("test");

        User emptyPassword = new User();
        emptyPassword.setId(2L);
        emptyPassword.setUsername("test");
        emptyPassword.setPassword("");

        User nullPassword = new User();
        nullPassword.setId(2L);
        nullPassword.setUsername("test");

        assertAll(() -> {
            assertThrows(NullPointerException.class, () -> userService.updateUser(new User()));
            assertThrows(IllegalArgumentException.class, () -> userService.updateUser(emptyUsername));
            assertThrows(NullPointerException.class, () -> userService.updateUser(nullId));
            assertThrows(IllegalArgumentException.class, () -> userService.updateUser(nullUsername));
            assertThrows(IllegalArgumentException.class, () -> userService.updateUser(emptyPassword));
            assertThrows(IllegalArgumentException.class, () -> userService.updateUser(nullPassword));
        });
    }

    @Test
    public void givenEmptyUsername_whenGetUserByUsername_thenThrowException() {
        assertAll(() -> {
            assertThrows(IllegalArgumentException.class, () -> userService.getUserByUsername(""));
            assertThrows(IllegalArgumentException.class, () -> userService.getUserByUsername(null));
        });
    }

    @Test
    public void givenNotExistingUsername_whenGetUserByUsername_thenThrowException() {
        assertThrows(ServiceException.class, () -> userService.getUserByUsername("not-existing-username"));
    }

    @Test
    public void givenExistingUsername_whenGetUserByUsername_thenReturnUser() {
        final User user = userService.getUserByUsername("test_user");

        assertEquals("test_user", user.getUsername());
    }
}
