package kz.zhanbolat.user;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import kz.zhanbolat.user.exception.ServiceException;
import kz.zhanbolat.user.exception.authorization.InvalidTokenException;
import kz.zhanbolat.user.service.AuthService;
import kz.zhanbolat.user.util.StringUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("test")
public class AuthServiceTest {
    @Autowired
    private AuthService authService;
    private List<String> expectedRoles;

    @BeforeEach
    public void init() {
        expectedRoles = new ArrayList<>();
    }

    @Test
    public void givenEmptyUsername_whenAuthenticateUser_thenThrowException() {
        assertAll(() -> {
            assertThrows(IllegalArgumentException.class, () -> authService.authenticateUser(null, "test"));
            assertThrows(IllegalArgumentException.class, () -> authService.authenticateUser("", "test"));
        });
    }

    @Test
    public void givenEmptyPassword_whenAuthenticateUser_thenThrowException() {
        assertAll(() -> {
            assertThrows(IllegalArgumentException.class, () -> authService.authenticateUser("test", null));
            assertThrows(IllegalArgumentException.class, () -> authService.authenticateUser("test", ""));
        });
    }

    @Test
    public void givenNotExistedUser_whenAuthenticateUser_thenThrowException() {
        assertAll(() -> {
            assertThrows(ServiceException.class, () -> authService.authenticateUser("test_user", "test"));
            assertThrows(ServiceException.class, () -> authService.authenticateUser("test", "test_password"));
        });
    }

    @Test
    public void givenExistingUser_whenAuthenticateUser_thenReturnToken() {
        String token = authService.authenticateUser("test_user", "test_password");

        assertFalse(StringUtil.isEmptyOrNull(token));
    }

    @Test
    public void givenEmptyToken_whenValidateToken_thenThrowException() {
        assertAll(() -> {
            assertThrows(IllegalArgumentException.class, () -> authService.getTokenClaims(null));
            assertThrows(IllegalArgumentException.class, () -> authService.getTokenClaims(""));
        });
    }

    @Test
    public void givenNotValidToken_whenValidateToken_thenThrowException() {
        String invalidToken = Jwts.builder().setId("test").setSubject("test").claim("key", "value").compact();

        assertThrows(InvalidTokenException.class, () -> authService.getTokenClaims(invalidToken));
    }

    @Test
    public void givenValidToken_whenValidateToken_thenReturnClamRoleStudent() {
        expectedRoles.add("STUDENT");
        String token = authService.authenticateUser("test_student", "test_student");

        Claims claims = authService.getTokenClaims(token);

        assertEquals(expectedRoles, claims.get("roles"));
    }

    @Test
    public void givenValidToken_whenValidateToken_thenReturnClamRoleLecturer() {
        expectedRoles.add("LECTURER");
        String token = authService.authenticateUser("test_lecturer", "test_lecturer");

        Claims claims = authService.getTokenClaims(token);

        assertEquals(expectedRoles, claims.get("roles"));
    }

    @Test
    public void givenValidToken_whenValidateToken_thenReturnClamRoleAdmin() {
        expectedRoles.add("ADMIN");
        String token = authService.authenticateUser("test_admin", "test_admin");

        Claims claims = authService.getTokenClaims(token);

        assertEquals(expectedRoles, claims.get("roles"));
    }

    @Test
    @SuppressWarnings({"unchecked"})
    public void givenValidToken_whenValidateToken_thenReturnClamRolesAdminAndLecturer() {
        expectedRoles.add("ADMIN");
        expectedRoles.add("LECTURER");
        String token = authService.authenticateUser("test_admin_lecturer", "test_admin_lecturer");

        Claims claims = authService.getTokenClaims(token);

        assertTrue(expectedRoles.containsAll((Collection<String>) claims.get("roles")));
    }
}
