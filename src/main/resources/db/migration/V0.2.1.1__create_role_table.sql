create table if not exists user_role(
    id smallserial primary key,
    name varchar(100) not null unique
);