alter table user_account
drop constraint user_account_user_role_fk;

alter table user_account
drop column role_id;