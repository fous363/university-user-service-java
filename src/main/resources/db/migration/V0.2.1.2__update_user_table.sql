alter table user_account
add column role_id smallint;

alter table user_account
add constraint user_account_user_role_fk foreign key (role_id) references user_role(id);