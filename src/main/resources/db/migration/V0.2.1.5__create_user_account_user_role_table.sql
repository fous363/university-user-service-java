create table if not exists user_account_user_role(
    user_account_id bigint not null,
    user_role_id smallint not null,
    foreign key (user_account_id) references user_account(id),
    foreign key (user_role_id) references user_role(id)
);