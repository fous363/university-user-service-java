package kz.zhanbolat.user.service.impl;

import kz.zhanbolat.user.entity.Role;
import kz.zhanbolat.user.entity.RoleType;
import kz.zhanbolat.user.entity.User;
import kz.zhanbolat.user.exception.ServiceException;
import kz.zhanbolat.user.repository.RoleRepository;
import kz.zhanbolat.user.repository.UserRepository;
import kz.zhanbolat.user.service.UserService;
import kz.zhanbolat.user.util.StringUtil;
import kz.zhanbolat.user.checker.EntityChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Objects;

/**
 * Implementation class for <code>UserService</code>
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private EntityChecker<User> userEntityChecker;

    /**
     * {@inheritDoc}
     *
     * Use userRepository to find user by id from database
     *
     * @see UserRepository
     *
     * @throws NullPointerException
     * @throws ServiceException
     */
    @Override
    public User getUser(Long userId) {
        Objects.requireNonNull(userId, "User id cannot be null");
        return userRepository.findById(userId).orElseThrow(() -> new ServiceException("User with such id doesn't exists"));
    }

    /**
     * {@inheritDoc}
     *
     *  If user doesn't exist and role exists, then the user will be created.
     *
     * @see StringUtil
     * @see UserRepository
     * @see kz.zhanbolat.user.checker.UserEntityChecker
     *
     * @throws ServiceException
     * @throws IllegalArgumentException
     * @throws NullPointerException
     */
    // todo: move the roles to a separate service
    @Override
    @Transactional
    public User createUser(User user, RoleType roleType) {
        userEntityChecker.checkOnCreate(user);
        Objects.requireNonNull(roleType, "Role type cannot be null.");

        if (Objects.nonNull(user.getId()) && userRepository.existsById(user.getId())) {
            throw new ServiceException("User already exists");
        }
        if (userRepository.existsByUsernameAndPassword(user.getUsername(), user.getPassword())) {
            throw new ServiceException("User with such username and password already exists");
        }

        Role role = roleRepository.findById(roleType.getId()).orElseThrow(() -> new ServiceException("No role for type " + roleType));
        user.setRoles(Collections.singletonList(role));
        return userRepository.save(user);
    }

    /**
     * {@inheritDoc}
     *
     * If user exists, then update the information.
     *
     * @see UserRepository
     * @see kz.zhanbolat.user.checker.UserEntityChecker
     */
    @Override
    @Transactional
    public User updateUser(User user) {
        userEntityChecker.checkOnUpdate(user);
        if (!userRepository.existsById(user.getId())) {
            throw new ServiceException("Cannot update not existing user");
        }

        return userRepository.save(user);
    }

    /**
     * {@inheritDoc}
     *
     * Use userRepository to find user by username from database
     *
     * @see StringUtil
     * @see UserRepository
     *
     * @throws NullPointerException
     * @throws ServiceException
     */
    @Override
    public User getUserByUsername(String username) {
        StringUtil.requireNotEmptyOrNull(username, "Username cannot be empty.");
        return userRepository.findByUsername(username).orElseThrow(() -> new ServiceException("User is not found."));
    }
}
