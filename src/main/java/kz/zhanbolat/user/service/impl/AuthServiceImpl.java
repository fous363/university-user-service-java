package kz.zhanbolat.user.service.impl;

import io.jsonwebtoken.*;
import io.jsonwebtoken.lang.Collections;
import kz.zhanbolat.user.entity.Role;
import kz.zhanbolat.user.entity.User;
import kz.zhanbolat.user.exception.ServiceException;
import kz.zhanbolat.user.exception.authorization.InvalidTokenException;
import kz.zhanbolat.user.repository.UserRepository;
import kz.zhanbolat.user.service.AuthService;
import kz.zhanbolat.user.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * The implementation of the <code>AuthService</code>
 */
@Service
public class AuthServiceImpl implements AuthService {
    private static final String ISSUER = "User-Service";
    @Autowired
    private UserRepository userRepository;
    @Value("${jwt.expiration}")
    private long expirationTimeInMinutes;
    private final SignatureAlgorithm signatureAlgorithm;
    private final Key signingKey;

    @Autowired
    public AuthServiceImpl(@Value("${jwt.secretKey}") String secretKey, @Value("${jwt.signature.algorithm}") String signatureAlgorithm) {
        this.signatureAlgorithm = SignatureAlgorithm.forName(signatureAlgorithm);
        byte[] secretKeyBytes = DatatypeConverter.parseBase64Binary(secretKey);
        signingKey = new SecretKeySpec(secretKeyBytes, this.signatureAlgorithm.getJcaName());
    }

    /**
     * {@inheritDoc}
     *
     * Generate the jwt token on successful authentication.
     *
     * @see UserRepository
     *
     * @throws IllegalArgumentException
     * @throws ServiceException
     */
    @Override
    public String authenticateUser(String username, String password) {
        StringUtil.requireNotEmptyOrNull(username, "Username cannot be empty.");
        StringUtil.requireNotEmptyOrNull(password, "Password cannot be empty.");

        User user = userRepository.findByUsernameAndPassword(username, password)
                .orElseThrow(() -> new ServiceException("User with such username or password does not exists"));
        LocalDateTime createdOn = LocalDateTime.now();

        JwtBuilder jwtBuilder = Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setIssuedAt(Date.from(createdOn.toInstant(ZoneOffset.UTC)))
                .setSubject(user.getUsername())
                .setIssuer(ISSUER)
                .signWith(signingKey, signatureAlgorithm)
                .setExpiration(Date.from(createdOn.plusMinutes(expirationTimeInMinutes).toInstant(ZoneOffset.UTC)));
        if (!Collections.isEmpty(user.getRoles())) {
            jwtBuilder.claim("roles", user.getRoles().stream()
                    .map(Role::getName)
                    .map(String::toUpperCase)
                    .collect(Collectors.toList()));
        }
        return jwtBuilder.compact();
    }

    /**
     * {@inheritDoc}
     *
     * @throws InvalidTokenException
     * @throws NullPointerException
     */
    @Override
    public Claims getTokenClaims(String token) {
        StringUtil.requireNotEmptyOrNull(token, "Token cannot be empty.");
        JwtParser jwtParser = Jwts.parserBuilder().setSigningKey(signingKey).build();
        if (!jwtParser.isSigned(token)) {
            throw new InvalidTokenException("Token is signed using another signing key.");
        }
        return jwtParser.parseClaimsJws(token).getBody();
    }
}
