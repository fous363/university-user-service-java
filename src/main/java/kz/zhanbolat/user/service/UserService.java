package kz.zhanbolat.user.service;

import kz.zhanbolat.user.entity.RoleType;
import kz.zhanbolat.user.entity.User;

/**
 * A service for user entity
 */
public interface UserService {
    /**
     * Return the user by id.
     *
     * @param userId - user's id, that's are greater than 0
     * @return a <code>User</code>
     */
    User getUser(Long userId);

    /**
     * Create user with specific role.
     *
     * @param user - user information. The id filed should be null or empty.
     * @param roleType - role of the user to create with
     * @see RoleType
     * @return a <code>User</code> with generated id
     */
    User createUser(User user, RoleType roleType);

    /**
     * Update the information about user.
     *
     * @param user - user information. The id filed shouldn't be empty or null
     * @return a <code>User</code> with updated information
     */
    User updateUser(User user);

    /**
     * Return the user by username.
     *
     * @param username - username
     * @return a <code>User</code>
     */
    User getUserByUsername(String username);
}
