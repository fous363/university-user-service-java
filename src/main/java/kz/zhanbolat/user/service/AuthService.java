package kz.zhanbolat.user.service;

import io.jsonwebtoken.Claims;

/**
 * Authentication and authorization class
 */
public interface AuthService {
    /**
     * Authenticate the user. If the user is authenticated successfully,
     * then the token will be returned.
     *
     * @param username - user's username
     * @param password - user's password
     * @return the token with user's roles
     */
    String authenticateUser(String username, String password);

    /**
     * Return token claims from specified token.
     *
     * @param token - token, which was signed with own secret key
     * @return claims from token
     */
    Claims getTokenClaims(String token);
}
