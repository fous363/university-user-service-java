package kz.zhanbolat.user.entity;

import java.util.Arrays;
import java.util.Objects;

/**
 * Default role types
 */
public enum RoleType {
    STUDENT(1, "student"), LECTURER(2, "lecturer");

    /**
     * Role's identifier
     */
    private final int id;
    /**
     * Role's name
     */
    private final String name;

    RoleType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    /**
     * Get role type by its name. If no role type is found, then exception will be thrown.
     *
     * @param name - role's name
     * @return RoleType
     */
    public static RoleType getRoleTypeByName(String name) {
        return Arrays.stream(RoleType.values())
                .filter(roleType -> Objects.equals(roleType.getName(), name))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No role with name " + name));
    }
}
