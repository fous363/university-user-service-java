package kz.zhanbolat.user.repository;

import kz.zhanbolat.user.entity.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * The repository class for <code>Role</code> entity class
 */
@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {
}
