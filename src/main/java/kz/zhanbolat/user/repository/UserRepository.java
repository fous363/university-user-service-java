package kz.zhanbolat.user.repository;

import kz.zhanbolat.user.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The repository class for <code>User</code> entity class
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    /**
     * Find the user by username and password.
     *
     * @param username - username
     * @param password - password
     * @return The <code>Optional</code> user object
     */
    Optional<User> findByUsernameAndPassword(String username, String password);

    /**
     * Find the user by username.
     *
     * @param username - username
     * @return The <code>Optional</code> user object
     */
    Optional<User> findByUsername(String username);

    /**
     * Check if the user with username and password already exists
     *
     * @param username - username
     * @param password - password
     * @return true, if user exists, otherwise false
     */
    boolean existsByUsernameAndPassword(String username, String password);
}
