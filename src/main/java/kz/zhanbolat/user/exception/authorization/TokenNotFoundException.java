package kz.zhanbolat.user.exception.authorization;

public class TokenNotFoundException extends AuthorizationException {

    public TokenNotFoundException(String message) {
        super(message);
    }
}
