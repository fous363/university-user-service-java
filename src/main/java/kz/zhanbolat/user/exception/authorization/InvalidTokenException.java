package kz.zhanbolat.user.exception.authorization;

public class InvalidTokenException extends AuthorizationException {
    public InvalidTokenException(String message) {
        super(message);
    }
}
