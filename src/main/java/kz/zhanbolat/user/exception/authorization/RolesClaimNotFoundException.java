package kz.zhanbolat.user.exception.authorization;

public class RolesClaimNotFoundException extends AuthorizationException {
    public RolesClaimNotFoundException(String message) {
        super(message);
    }
}
