package kz.zhanbolat.user.exception.authorization;

public abstract class AuthorizationException extends RuntimeException {
    public AuthorizationException(String message) {
        super(message);
    }
}
