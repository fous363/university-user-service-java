package kz.zhanbolat.user.util;

import java.util.Objects;

/**
 * Util class for string class
 */
public final class StringUtil {
    private static final String EMPTY_STRING = "";

    /**
     * Check if string is null or empty.
     *
     * @param string - string
     * @return true, if string is empty or null, otherwise false
     */
    public static boolean isEmptyOrNull(String string) {
        return Objects.isNull(string) || Objects.equals(EMPTY_STRING, string);
    }

    /**
     * Throw exception if string is empty or null.
     *
     * @param string - string
     * @param msg - exception message
     */
    public static void requireNotEmptyOrNull(String string, String msg) {
        if (isEmptyOrNull(string)) {
            throw new IllegalArgumentException(msg);
        }
    }
}
