package kz.zhanbolat.user.checker;

/**
 * Entity checker to check if object passes the specific conditions.
 * If not, then exception will be thrown.
 *
 * @param <T> - entity class for checking
 */
public interface EntityChecker<T> {
    /**
     * Check if object can be used for creating.
     *
     * @param entity - object
     */
    void checkOnCreate(T entity);

    /**
     * Check if object can be used for updating.
     *
     * @param entity - object
     */
    void checkOnUpdate(T entity);
}
