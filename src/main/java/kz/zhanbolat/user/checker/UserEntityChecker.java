package kz.zhanbolat.user.checker;

import kz.zhanbolat.user.entity.User;
import kz.zhanbolat.user.util.StringUtil;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * A entity checker for <code>User</code> entity class
 *
 * @see EntityChecker
 */
@Component
public class UserEntityChecker implements EntityChecker<User> {

    /**
     * {@inheritDoc}
     *
     * Check if user object can be used for creating.
     *
     * @see StringUtil
     *
     * @throws IllegalArgumentException
     */
    @Override
    public void checkOnCreate(User entity) {
        Objects.requireNonNull(entity, "User cannot be null.");
        StringUtil.requireNotEmptyOrNull(entity.getUsername(), "Username cannot be null or empty.");
        StringUtil.requireNotEmptyOrNull(entity.getPassword(), "Password cannot be null or empty.");
    }

    /**
     * {@inheritDoc}
     *
     * Check if user object can be used for updating.
     *
     * @see StringUtil
     *
     * @throws IllegalArgumentException
     */
    @Override
    public void checkOnUpdate(User entity) {
        Objects.requireNonNull(entity, "User cannot be null.");
        if (Objects.requireNonNull(entity.getId(), "Id cannot be null or empty") <= 0L) {
            throw new IllegalArgumentException("Id cannot be below or equal to 0.");
        }
        StringUtil.requireNotEmptyOrNull(entity.getUsername(), "Username cannot be null or empty.");
        StringUtil.requireNotEmptyOrNull(entity.getPassword(), "Password cannot be null or empty.");
    }
}
