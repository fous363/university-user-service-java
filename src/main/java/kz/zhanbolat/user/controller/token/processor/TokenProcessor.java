package kz.zhanbolat.user.controller.token.processor;

/**
 * Processor for token
 * @param <T> - the class where token is stored
 * @param <K> - the result of the processing
 */
@FunctionalInterface
public interface TokenProcessor<T, K> {
    /**
     * Processing the token.
     *
     * @param t - token
     * @return Result of the processing
     */
    K process(T t);
}
