package kz.zhanbolat.user.controller.token.generator;

import kz.zhanbolat.user.util.StringUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Access token generator to store in cookie
 */
@Component
public class AccessTokenCookieGenerator implements TokenCookieGenerator<String, String> {
    @Value("${server.domain}")
    private String domain;
    @Value("${server.cookie.token.name}")
    private String cookieName;
    @Value("${server.cookie.sameSite.value}")
    private String sameSiteValue;
    @Value("${jwt.expiration}")
    private long expirationTimeInMinutes;
    @Value("${server.cookie.secure:false}")
    private boolean isSecure;

    /**
     * {@inheritDoc}
     *
     * Generate the cookie representation as <code>string</code> to store the access token
     * @param token - token
     * @return Cookie representation
     *
     * @see StringUtil
     *
     * @throws IllegalArgumentException
     */
    @Override
    public String generate(String token) {
        StringUtil.requireNotEmptyOrNull(token, "Token cannot be empty");
        StringBuilder builder = new StringBuilder()
                .append(cookieName).append("=").append(token)
                .append("; Max-Age=").append(expirationTimeInMinutes * 60)
                .append("; Domain=").append(domain)
                .append("; Path=/");
        if (isSecure) {
            builder.append("; Secure");
        }
        builder.append("; HttpOnly");
        builder.append("; SameSite=").append(sameSiteValue);
        return builder.toString();
    }
}
