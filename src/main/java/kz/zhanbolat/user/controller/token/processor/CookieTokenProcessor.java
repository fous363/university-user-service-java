package kz.zhanbolat.user.controller.token.processor;

import io.jsonwebtoken.Claims;
import kz.zhanbolat.user.exception.authorization.TokenNotFoundException;
import kz.zhanbolat.user.exception.authorization.RolesClaimNotFoundException;
import kz.zhanbolat.user.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Objects;

@Component
public class CookieTokenProcessor implements TokenProcessor<HttpServletRequest, Claims> {
    @Value("${server.cookie.token.name}")
    private String tokenName;
    @Autowired
    private AuthService authService;

    /**
     * {@inheritDoc}
     *
     * Process the request to get the cookie with token to get the token claims.
     *
     * @param request - http request with token as cookie
     * @return Claims of the token
     *
     * @see AuthService
     *
     * @throws TokenNotFoundException
     * @throws RolesClaimNotFoundException
     */
    @Override
    public Claims process(HttpServletRequest request) {
        if (Objects.isNull(request.getCookies()) || request.getCookies().length < 1) {
            throw new TokenNotFoundException("Authorization token with name " + tokenName + " was not found.");
        }
        Cookie authorizationToken = Arrays.stream(request.getCookies())
                .filter(cookie -> Objects.equals(cookie.getName(), tokenName))
                .findFirst()
                .orElseThrow(() -> new TokenNotFoundException("Authorization token with name " + tokenName + " was not found."));
        Claims claims = authService.getTokenClaims(authorizationToken.getValue());
        if (!claims.containsKey("roles")) {
            throw new RolesClaimNotFoundException("Validated token doesn't have clams with key 'role'.");
        }
        return claims;
    }
}
