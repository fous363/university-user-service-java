package kz.zhanbolat.user.controller.token.generator;

/**
 * Cookie generator to store the token
 *
 * @param <T> - token
 * @param <K> - the cookie representation
 */
@FunctionalInterface
public interface TokenCookieGenerator<T, K> {
    /**
     * Generate the cookie representation to store the cookie.
     *
     * @param t - token
     * @return - cookie representation
     */
    K generate(T t);
}
