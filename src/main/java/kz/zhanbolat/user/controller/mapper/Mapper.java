package kz.zhanbolat.user.controller.mapper;

/**
 * Mapper to map the entity <code>T<code/> to entity <code>K</code>
 * @param <T> - entity
 * @param <K> - entity
 */
@FunctionalInterface
public interface Mapper<T, K> {
    /**
     * Map the entity <code>T</code> to entity <code>K</code>
     *
     * @param t - entity
     * @return return entity <code>K</code>
     */
    K map(T t);
}
