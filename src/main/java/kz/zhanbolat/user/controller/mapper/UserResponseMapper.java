package kz.zhanbolat.user.controller.mapper;

import kz.zhanbolat.user.controller.dto.UserResponse;
import kz.zhanbolat.user.entity.User;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Mapper to map the <code>User</code> to <code>UserResponse</code>
 */
@Component
public class UserResponseMapper implements Mapper<User, UserResponse> {

    /**
     * {@inheritDoc}
     *
     * Map the <code>User</code> to <code>UserResponse</code>
     *
     * @param user - entity <code>User</code>
     * @return entity <code>UserResponse</code>
     *
     * @see UserResponse
     *
     * @throws NullPointerException
     */
    @Override
    public UserResponse map(User user) {
        Objects.requireNonNull(user, "User cannot be null.");
        return new UserResponse(user.getId(), user.getUsername(), user.getRoles());
    }
}
