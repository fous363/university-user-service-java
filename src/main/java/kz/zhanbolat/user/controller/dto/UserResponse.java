package kz.zhanbolat.user.controller.dto;

import kz.zhanbolat.user.entity.Role;

import java.util.List;

/**
 * DTO class to return the user in order to hide the password
 */
public class UserResponse {
    /**
     * User's identifier
     */
    private final Long id;
    /**
     * User's username
     */
    private final String username;
    /**
     * User's roles
     */
    private final List<Role> roles;

    public UserResponse(Long id, String username, List<Role> roles) {
        this.id = id;
        this.username = username;
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public List<Role> getRoles() {
        return roles;
    }
}
