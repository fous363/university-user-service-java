package kz.zhanbolat.user.controller.dto;

/**
 * Error response class for client
 */
public class ErrorResponse {
    /**
     * Error message
     */
    private String message;

    public ErrorResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
