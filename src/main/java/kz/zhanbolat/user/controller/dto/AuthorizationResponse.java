package kz.zhanbolat.user.controller.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Authorization response class for client
 */
public class AuthorizationResponse {
    /**
     * flag to determine if user is authorized.
     */
    private boolean isAuthorized;
    /**
     * Authorization subject (optional)
     */
    private String authorizationSubject;
    /**
     * User's roles
     */
    private List<String> authorizationRoles = new ArrayList<>();

    public AuthorizationResponse(boolean isAuthorized) {
        this.isAuthorized = isAuthorized;
    }

    public AuthorizationResponse(boolean isAuthorized, String authorizationSubject, List<String> authorizationRoles) {
        this.isAuthorized = isAuthorized;
        this.authorizationSubject = authorizationSubject;
        this.authorizationRoles = authorizationRoles;
    }

    public boolean isAuthorized() {
        return isAuthorized;
    }

    public void setAuthorized(boolean authorized) {
        isAuthorized = authorized;
    }

    public String getAuthorizationSubject() {
        return authorizationSubject;
    }

    public void setAuthorizationSubject(String authorizationSubject) {
        this.authorizationSubject = authorizationSubject;
    }

    public List<String> getAuthorizationRoles() {
        return authorizationRoles;
    }

    public void setAuthorizationRoles(List<String> authorizationRoles) {
        this.authorizationRoles = authorizationRoles;
    }
}
