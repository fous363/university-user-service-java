package kz.zhanbolat.user.controller;

import io.jsonwebtoken.Claims;
import kz.zhanbolat.user.controller.dto.AuthorizationResponse;
import kz.zhanbolat.user.controller.dto.ErrorResponse;
import kz.zhanbolat.user.controller.dto.UserResponse;
import kz.zhanbolat.user.controller.mapper.Mapper;
import kz.zhanbolat.user.controller.token.generator.TokenCookieGenerator;
import kz.zhanbolat.user.controller.token.processor.TokenProcessor;
import kz.zhanbolat.user.entity.RoleType;
import kz.zhanbolat.user.entity.User;
import kz.zhanbolat.user.exception.ServiceException;
import kz.zhanbolat.user.exception.authorization.TokenNotFoundException;
import kz.zhanbolat.user.exception.authorization.InvalidTokenException;
import kz.zhanbolat.user.exception.authorization.RolesClaimNotFoundException;
import kz.zhanbolat.user.service.AuthService;
import kz.zhanbolat.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Rest controller class for user.
 */
@RestController
@RequestMapping("/api/user")
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserService userService;
    @Autowired
    private AuthService authService;
    @Autowired
    private TokenCookieGenerator<String, String> cookieGenerator;
    @Autowired
    private TokenProcessor<HttpServletRequest, Claims> cookieTokenProcessor;
    @Autowired
    private Mapper<User, UserResponse> userResponseMapper;

    @Value("${server.cookie.token.name}")
    private String tokenName;

    /**
     * Get the user by user's id.
     * 
     * @param userId - user's id
     * @return the user
     *
     * @see UserService
     * @see kz.zhanbolat.user.controller.mapper.UserResponseMapper
     */
    @GetMapping("/{userId}")
    public UserResponse getUser(@PathVariable("userId") Long userId) {
        return userResponseMapper.map(userService.getUser(userId));
    }

    /**
     * Create the user with specific role.
     *
     * @param roleType - role of the user
     * @param user - user
     * @return the created user
     *
     * @see UserService
     * @see kz.zhanbolat.user.controller.mapper.UserResponseMapper
     */
    @PostMapping("/{roleType}")
    public UserResponse createUser(@PathVariable("roleType") String roleType, @RequestBody User user) {
        return userResponseMapper.map(userService.createUser(user, RoleType.getRoleTypeByName(roleType)));
    }

    /**
     * Update the user's information
     *
     * @param user - user
     * @return the updated user
     *
     * @see UserService
     * @see kz.zhanbolat.user.controller.mapper.UserResponseMapper
     */
    @PutMapping
    public UserResponse updateUser(@RequestBody User user) {
        return userResponseMapper.map(userService.updateUser(user));
    }

    /**
     * Authenticate the user. Return the user with access token in the cookie.
     *
     * @param user - the user information
     * @param response - the http response to set the access token in cookie
     * @return the user
     *
     * @see AuthService
     * @see kz.zhanbolat.user.controller.token.generator.AccessTokenCookieGenerator
     * @see kz.zhanbolat.user.controller.mapper.UserResponseMapper
     */
    @PostMapping("/auth")
    public UserResponse authenticateUser(@RequestBody User user, HttpServletResponse response) {
        String token = authService.authenticateUser(user.getUsername(), user.getPassword());
        String cookie = cookieGenerator.generate(token);
        response.setHeader("Set-Cookie", cookie);
        return userResponseMapper.map(userService.getUserByUsername(user.getUsername()));
    }

    /**
     * Return the permissions of the user from the token.
     * @param request - http request to get the token
     * @return the authorization response of permissions for client
     *
     * @see kz.zhanbolat.user.controller.token.processor.CookieTokenProcessor
     */
    @PostMapping("/validate/token")
    @SuppressWarnings({"unchecked"})
    public AuthorizationResponse validateAuthorizationToken(HttpServletRequest request) {
        Claims claims = cookieTokenProcessor.process(request);
        List<String> authorizationRoles = ((List<String>) claims.get("roles"));
        return new AuthorizationResponse(true, claims.getSubject(), authorizationRoles);
    }

    /**
     * Handle all types of exceptions to response for client.
     *
     * @param exception - exception
     * @return the error response with specific message
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler
    public ErrorResponse handleException(Exception exception) {
        logger.error("Caught exception: " + exception);
        return new ErrorResponse("Internal server error.");
    }

    /**
     * Handle illegal argument exception types of exceptions to response for client.
     *
     * @param exception - exception
     * @return the error response with exception message
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ServiceException.class)
    public ErrorResponse handleArgumentException(ServiceException exception) {
        logger.error("Caught service exception: " + exception);
        return new ErrorResponse(exception.getMessage());
    }

    /**
     * Handle authorization types of exceptions to response for client.
     *
     * @param exception - exception
     * @return the authorization response with flag 'isAuthorized' to false.
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({InvalidTokenException.class, RolesClaimNotFoundException.class, TokenNotFoundException.class})
    public AuthorizationResponse handleAuthorizationExceptions(Exception exception) {
        logger.error("Caught exception during authorization: " + exception);
        return new AuthorizationResponse(false);
    }
}
