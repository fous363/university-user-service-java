package kz.zhanbolat.user.controller.filter;

import io.jsonwebtoken.Claims;
import kz.zhanbolat.user.controller.token.processor.CookieTokenProcessor;
import kz.zhanbolat.user.exception.authorization.TokenNotFoundException;
import kz.zhanbolat.user.exception.authorization.InvalidTokenException;
import kz.zhanbolat.user.exception.authorization.RolesClaimNotFoundException;
import kz.zhanbolat.user.service.AuthService;
import kz.zhanbolat.user.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Filter for authorization jwt
 */
@Component
public class JwtAuthorizationFilter extends OncePerRequestFilter {
    private static final Logger logger = LoggerFactory.getLogger(JwtAuthorizationFilter.class);
    @Value("${server.cookie.token.name}")
    private String tokenName;
    @Autowired
    private AuthService authService;
    @Autowired
    private CookieTokenProcessor cookieTokenProcessor;

    /**
     * {@inheritDoc}
     *
     * Process the cookie from request to get the claims,
     * and get the roles from the claims to set the <code>SecurityContext</code> to use in authorization process.
     * If the exception is acquired, then response with status 401(<code>HttpStatus.UNAUTHORIZED</code>).
     *
     * @see CookieTokenProcessor
     */
    @Override
    @SuppressWarnings({"unchecked"})
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (Objects.nonNull(request.getCookies())
                && request.getCookies().length > 0
                && isTokenPresent(request.getCookies())) {
            try {
                Claims claims = cookieTokenProcessor.process(request);

                List<String> authorizationRoles = ((List<String>) claims.get("roles"));

                UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(claims.getSubject(),
                        null, authorizationRoles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
                SecurityContextHolder.getContext().setAuthentication(auth);
            } catch (TokenNotFoundException | InvalidTokenException | RolesClaimNotFoundException exception) {
                logger.error("Caught authorization exception", exception);
                response.sendError(HttpStatus.UNAUTHORIZED.value());
                return;
            }
        }
        filterChain.doFilter(request, response);
    }

    /**
     * Check if the token presents in the cookies.
     *
     * @param cookies - cookies
     * @return true, if token is in the cookies, otherwise false
     */
    private boolean isTokenPresent(Cookie[] cookies) {
        return Arrays.stream(cookies).anyMatch(cookie -> cookie.getName().equalsIgnoreCase(tokenName));
    }

    /**
     * {@inheritDoc}
     *
     * Ignore the filtering on specific requests.
     * Ignore the POST request to url '/api/user/validate/token'.
     */
    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        String path;
        // add this condition cause of test failing for controller,
        // because the value servlet path is always empty
        if (StringUtil.isEmptyOrNull(request.getServletPath())) {
            path = request.getPathInfo();
        } else {
            path = request.getServletPath();
        }
        return HttpMethod.POST.matches(request.getMethod()) && path.contains("/validate/token");
    }
}
