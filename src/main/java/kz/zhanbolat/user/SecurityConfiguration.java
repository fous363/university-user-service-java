package kz.zhanbolat.user;

import kz.zhanbolat.user.controller.filter.JwtAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private JwtAuthorizationFilter jwtAuthorizationFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .formLogin().disable()
                .logout().disable()
                .csrf().disable()
                .authorizeRequests(authorizeRequestsCustomizer -> authorizeRequestsCustomizer
                        .antMatchers(HttpMethod.POST, "/api/user/**", "/api/user/auth", "/api/user/validate/token").permitAll()
                        .antMatchers(HttpMethod.PUT, "/api/user/").hasAnyAuthority("ADMIN", "STUDENT", "LECTURER")
                        .antMatchers(HttpMethod.GET, "/api/user/*").hasAnyAuthority("ADMIN", "STUDENT", "LECTURER")
                        .and().addFilterBefore(jwtAuthorizationFilterBean().getFilter(), UsernamePasswordAuthenticationFilter.class))
                .cors().disable();
    }

    @Bean
    public FilterRegistrationBean jwtAuthorizationFilterBean() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(jwtAuthorizationFilter);
        registration.addUrlPatterns("/api/user**");
        registration.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return registration;
    }
}
