# University-User-Service-Java

This is the user service project.

Endpoints:

    - GET /api/user/{userId} - to get user by id. Required the authorization.
    - POST /api/user/{roleType} - to create user with role.
    - PUT /api/user/ - to update the user. Required the authorization.
    - POST /api/user/auth - to authentication the user.
    - POST /api/user/validate/token - to validate the token.

For local environment:

    - Update the application-dev.yml
    - Run "gradle bootRun --args="--spring.profiles.active=dev""

For production environment see:

    - application-prod.yml
    - Dockerfile

Update them if needed